from flask import Flask,render_template
import threading
import serial
import configparser
import paho.mqtt.client as paho
import time
import logging

app = Flask(__name__)

@app.route("/")
def hello_world():
    return render_template('index.html', bvoltage=actdata["bvoltage"], bcurrent=actdata["bcurrent"], bpower=actdata["bpower"], pvvoltage=actdata["pvvoltage"], pvpower=actdata["pvpower"], iload=actdata["iload"], pload=actdata["pload"], pvpowermaxtoday=actdata["pvpowermaxtoday"], pvpowermaxyesterday=actdata["pvpowermaxyesterday"], yieldtotal=actdata["yieldtotal"], yieldtoday=actdata["yieldtoday"], yieldyesterday=actdata["yieldyesterday"], opstate=actdata["opstate"], error=actdata["error"], loadmode=actdata["loadmode"], mppt=actdata["mppt"])

def read_mppt(actdata):
    ser = serial.Serial(
            port = config["default"]["serialport"],
            baudrate=19200,
            )
    while True:
        actline = str(ser.readline())
        if actline.find("b\'V\\t") != -1:
            actdata["bvoltage"] = int(actline.split("b\'V\\t")[1].split("\\r")[0]) / 1000
        elif actline.find("b\'I\\t") != -1:
            actdata["bcurrent"] = int(actline.split("b\'I\\t")[1].split("\\r")[0]) / 1000
            actdata["bpower"] = round(actdata["bvoltage"] * actdata["bcurrent"], 2)
        elif actline.find("b\'VPV\\t") != -1:
            actdata["pvvoltage"] = int(actline.split("b\'VPV\\t")[1].split("\\r")[0]) / 1000
        elif actline.find("b\'PPV\\t") != -1:
            actdata["pvpower"] = int(actline.split("b\'PPV\\t")[1].split("\\r")[0])
        elif actline.find("b\'IL\\t") != -1:
            actdata["iload"] = int(actline.split("b\'IL\\t")[1].split("\\r")[0]) / 1000
            actdata["pload"] = round(actdata["bvoltage"] * actdata["iload"], 2)
        elif actline.find("b\'H19\\t") != -1:
            actdata["yieldtotal"] = int(actline.split("b\'H19\\t")[1].split("\\r")[0]) * 10
        elif actline.find("b\'H20\\t") != -1:
            actdata["yieldtoday"] = int(actline.split("b\'H20\\t")[1].split("\\r")[0]) * 10
        elif actline.find("b\'H21\\t") != -1:
            actdata["pvpowermaxtoday"] = int(actline.split("b\'H21\\t")[1].split("\\r")[0])
        elif actline.find("b\'H22\\t") != -1:
            actdata["yieldyesterday"] = int(actline.split("b\'H22\\t")[1].split("\\r")[0]) * 10
        elif actline.find("b\'H23\\t") != -1:
            actdata["pvpowermaxyesterday"] = int(actline.split("b\'H23\\t")[1].split("\\r")[0])
        elif actline.find("b\'CS\\t") != -1:
            actdata["opstate"] = int(actline.split("b\'CS\\t")[1].split("\\r")[0])
        elif actline.find("b\'ERR\\t") != -1:
            actdata["error"] = int(actline.split("b\'ERR\\t")[1].split("\\r")[0])
        elif actline.find("b\'LOAD\\t") != -1:
            actdata["loadmode"] = actline.split("b\'LOAD\\t")[1].split("\\r")[0]
        elif actline.find("b\'MPPT\\t") != -1:
            actdata["mppt"] = int(actline.split("b\'MPPT\\t")[1].split("\\r")[0])
    #wird nie erreicht
    ser.close()

def mqtt_connect():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Successfully conencted to MQTT broker")
        else:
            print("Failed to connect to MQTT broker, return code %d\n", rc)

    def on_disconnect(client, userdata, rc):
        first_reconnect_delay = 1
        reconnect_rate = 2
        max_reconnect_delay = 60
        logging.info("disconnected from MQTT broker with result code: %s", rc)
        reconnect_count, reconnect_delay = 0, first_reconnect_delay
        while True:
            logging.info("Reconnect to MQTT broker in %d seconds…", reconnect_delay)
            time.sleep(reconnect_delay)

            try:
                client.reconnect()
                logging.info("Reconnected to MQTT broker successfully")
                return
            except Exception as err:
                logging.error("%s. Reconnect to MQTT broker failed. Retrying…", err)
            reconnect_delay *= reconnect_rate
            reconnect_delay = min(reconnect_delay, max_reconnect_delay)

    #todo: clientid = seriennummer
    mqttclient = paho.Client("VEMPPT")
    mqttclient.on_connect = on_connect
    mqttclient.on_disconnect = on_disconnect
    mqttclient.connect(config["mqtt"]["broker"], int(config["mqtt"]["port"]))
    return mqttclient

def mqtt_publish(mqttclient):
    while True:
        mqttclient.publish(config["mqtt"]["topic"], "labsolar pvvoltage=" + str(actdata["pvvoltage"]) + ",pvpower=" + str(actdata["pvpower"]) + ",bvoltage=" + str(actdata["bvoltage"]) + ",iload=" + str(actdata["iload"]) + ",bcurrent=" + str(actdata["bcurrent"]) + ",yieldtotal=" + str(actdata["yieldtotal"]) + ",yieldtoday=" + str(actdata["yieldtoday"]) + ",yieldyesterday=" + str(actdata["yieldyesterday"]) + ",pvpowermaxtoday=" + str(actdata["pvpowermaxtoday"]) + ",pvpowermaxyesterday=" + str(actdata["pvpowermaxyesterday"]) + ",opstate=" + str(actdata["opstate"]) + ",error=" + str(actdata["error"]) + ",mppt=" + str(actdata["mppt"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "bvoltage", str(actdata["bvoltage"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "bcurrent", str(actdata["bcurrent"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "bpower", str(actdata["bpower"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "pvvoltage", str(actdata["pvvoltage"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "pvpower", str(actdata["pvpower"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "iload", str(actdata["iload"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "pload", str(actdata["pload"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "yieldtotal", str(actdata["yieldtotal"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "yieldtoday", str(actdata["yieldtoday"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "pvpowermaxtoday", str(actdata["pvpowermaxtoday"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "yieldyesterday", str(actdata["yieldyesterday"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "pvpowermaxyesterday", str(actdata["pvpowermaxyesterday"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "opstate", str(actdata["opstate"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "error", str(actdata["error"]))
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "loadmode", actdata["loadmode"])
        #mqttclient.publish(config["mqtt"]["topic"] + "/" + "mppt", str(actdata["mppt"]))
        time.sleep(10)

def mqtt_thread():
    client = mqtt_connect()
    mqtt_publish(client)

if __name__ == "__main__":
    config = configparser.ConfigParser()
    config.read("solar.conf")

    actdata = {
            "bvoltage": 0,
            "bcurrent": 0,
            "bpower": 0,
            "pvvoltage": 0,
            "pvpower": 0,
            "iload": 0,
            "pload": 0,
            "pvpowermaxtoday": 0,
            "pvpowermaxyesterday": 0,
            "yieldtotal": 0,
            "yieldtoday": 0,
            "yieldyesterday": 0,
            "mppt": 0,
            "error": 0,
            "loadmode": "ON",
            "opstate": 0
            }

    if config["webserver"]["enable"] == "yes":
        print("Webserver enabled, starting flask app")
        flaskthread = threading.Thread(daemon=True, target=app.run, kwargs={"host": "127.0.0.1"})
        flaskthread.start()
    else:
        print("Webserver not enabled")

    if config["mqtt"]["enable"] == "yes":
        print("MQTT enabled, starting mqtt thread")
        mqttthread = threading.Thread(daemon=True, target=mqtt_thread)
        mqttthread.start()
    else:
        print("MQTT not enabled")

    read_mppt(actdata)
