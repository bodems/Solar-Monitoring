# Solar-Monitoring

Hard- and Software for reading measurement data from a Victron Energy MPPT solar charger. The values are read and presented at a website

## Hardware
The Victron Energy MPPT solar chargers (BlueSolar or SmartSolar) have a serial interface, called VE.Direct. The connector is a JST 4pin connector. As the TTL level is 5V, you probably need a level shifter or you can use an expensive VE.Direct USB cable. I use a Banana Pi M2 zero with a level shifter (ST1167), but it should work on any other hardware.

## Software
This software is written in python. It starts after boot with a systemd service and continiously reads values from the MPTT tracker sent via the serial interface. In the config file you can enable the webserver and MQTT service. If you want the website to be accessable from outside you need a reverse proxy. For now, the MQTT service doesn't support authorization or TLS. The user running the service needs access to the serial interface (dialout user group).

### Installation
1. Copy files from software to `/opt/solar/`
2. Create a virtual environment in `/opt/solar/` and install Flask, pyserial and paho-mqtt in it
3. Copy 'solar.service' to `/etc/systemd/system` and change the User variable to your actual user
4. Enable the service with `sudo systemctl enable solar.service`
5. Change the serial port in `solar.conf` to your actual serial port and configure the services you want
6. If you use the webserver feature: Install and configure a reverse proxy, like nginx, and point it to http://localhost:5000
7. Start the service with `sudo systemctl start solar.service`

That's it!

### Issues
MQTT reconnection is not stable yet
